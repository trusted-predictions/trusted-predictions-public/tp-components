import React from 'react';
import { Col, Row, Table } from 'reactstrap';

const playerDetails = {
  name: 'Virat Kohli',
  type: 'BAT',
  out: 'rou out (G Maxwell)',
  runs: '30',
  balls: '32',
  fours: '4',
  sizes: '6',
  sr: '124.6',
};

const BatsmanRowItem = ({ item = playerDetails }) => {
  return (
    <tbody>
      <tr>
        <th scope="row">{item.name}</th>
        <td style={{ minWidth: 180 }}>{item.out}</td>
        <td>{item.runs}</td>
        <td>{item.balls}</td>
        <td>{item.fours}</td>
        <td>{item.sizes}</td>
        <td>{item.sr}</td>
      </tr>
    </tbody>
  );
};
const BowlerRowItem = ({ item }) => {
  return (
    <tbody>
      <tr>
        <th scope="row">'Adam Zampa</th>
        <td>4.2</td>
        <td>25</td>
        <td>2</td>
        <td>1</td>
        <td>8.25</td>
      </tr>
    </tbody>
  );
};

const Scorecard = () => {
  return (
    <Row>
      <Col md="12" lg="10">
        <h5 className="text-primary mb-1">Innings 1 - Team A</h5>
        <h5 className="text-bold">Total 180/5 (19.5)</h5>
        <Table responsive>
          <thead className="bg-light">
            <tr>
              <th>Batsman</th>
              <th className="text-center" style={{ minWidth: 180 }}>
                Dismissal
              </th>
              <th className="text-center">R</th>
              <th className="text-center">B</th>
              <th className="text-center">4s</th>
              <th className="text-center">6s</th>
              <th className="text-center">SR</th>
            </tr>
          </thead>
          {[...Array(7)].map((item) => (
            <BatsmanRowItem key={item} />
          ))}
        </Table>
        <div className="bg-light p-1 mb-1">
          <h6 className="m-0 tex-light">
            Extras: 7 , no-ball: 7, leg-buy: 3, wides:1
          </h6>
        </div>
        <Table responsive>
          <thead className="bg-light">
            <tr>
              <th>Bowler</th>
              <th className="text-center">Overs</th>
              <th className="text-center">Runs</th>
              <th className="text-center">Wickets</th>
              <th className="text-center">Maidens</th>
              <th className="text-center">Eco</th>
            </tr>
          </thead>
          {[...Array(5)].map((item) => (
            <BowlerRowItem key={item} />
          ))}
        </Table>
      </Col>
    </Row>
  );
};

export default Scorecard;
