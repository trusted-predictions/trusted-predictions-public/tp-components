import React from 'react';
import { Col, Row, Table } from 'reactstrap';

const Summery = () => {
  return (
    <Row className="mb-1">
      <Col md="6" sm="12" lg="5" xl="5">
        <Table responsive>
          <thead>
            <tr>
              <th>Player</th>
              <th className="text-center">Runs(B)</th>
              <th className="text-center">4s</th>
              <th className="text-center">6s</th>
              <th className="text-center">SR</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Rohit Sharma</th>
              <td>88(70)</td>
              <td>4</td>
              <td>4</td>
              <td>125.7</td>
            </tr>
            <tr>
              <th scope="row">Manish Pandey</th>
              <td>48(40)</td>
              <td>4</td>
              <td>2</td>
              <td>120</td>
            </tr>
          </tbody>
        </Table>
        <hr className="m-0" />
        <Table responsive>
          <thead>
            <tr>
              <th>Player</th>
              <th className="text-center">Over</th>
              <th className="text-center">M</th>
              <th className="text-center">R</th>
              <th className="text-center">W</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Adam Zampa</th>
              <td>8.5</td>
              <td>1</td>
              <td>40</td>
              <td>1</td>
            </tr>
          </tbody>
        </Table>
      </Col>
      <Col
        md="6"
        sm="12"
        lg="5"
        xl="5"
        className="d-flex flex-column justify-content-between"
        style={{ minHeight: 210 }}
      >
        <div>
          <p className="m-0 text-primary">Previous Balls</p>
          <h6 style={{ marginTop: 4, marginBottom: 0 }}>
            1 2 4 0 1 2 | W 1 0 0 2 4 | 6 6 0 0 W 6
          </h6>
        </div>
        <div>
          <h6 className="m-0 text-primary">
            Last Wicket:{' '}
            <span className="text-dark text-bold">KL Rahul 45(50)</span>{' '}
            <span className="text-danger"> b Zampa c MAxwell</span>{' '}
          </h6>
        </div>
        <div className="d-flex align-items-center justify-content-between ">
          <h6 className="m-0">Run Rate:7.33 RPO</h6>
          <h6 className="m-0 text-primary">
            Extras <span>7</span>
          </h6>
        </div>
        <div className="">
          <p className="m-0 mb-1 text-primary">Projected Score</p>
          <div className="d-flex justify-content-between">
            <h6 className="m-0">360 (7.33 RPO)</h6>
            <h6 className="m-0">360 (7.33 RPO)</h6>
            <h6 className="m-0">360 (7.33 RPO)</h6>
          </div>
        </div>
      </Col>
      <Col lg="2" />
    </Row>
  );
};

export default Summery;
