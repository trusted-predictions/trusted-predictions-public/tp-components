import React, { useState } from 'react';
import Summery from './Summery';
import Scorecard from './Scorecard';
import Commentary from './Commentry';
import Analytics from './Analytics';
import Tabs from '../match/Tabs';

const options = ['Commentary', 'Scorecard', 'Analytics'];

const MatchDetails = () => {
  const [selectedIndex, setSelectedIndex] = useState(0);

  const onTabPress = (index) => {
    setSelectedIndex(index);
  };

  return (
    <>
      <Summery />

      <Tabs
        items={options}
        onPress={onTabPress}
        selectedIndex={selectedIndex}
      />
      {selectedIndex === 0 ? (
        <Commentary />
      ) : selectedIndex === 1 ? (
        <Scorecard />
      ) : (
        <Analytics />
      )}
    </>
  );
};

export default MatchDetails;
