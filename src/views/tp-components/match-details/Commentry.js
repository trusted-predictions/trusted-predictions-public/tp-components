import React from 'react';
import { Col, Row } from 'reactstrap';

const data = [
  {
    ball: 44.1,
    text: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp`,
  },
  {
    ball: 44.2,
    text: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp`,
  },
  {
    ball: 44.3,
    text: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp`,
  },
  {
    ball: 44.4,
    text: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp`,
  },
  {
    ball: 44.5,
    text: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp`,
  },
  {
    ball: 44.6,
    text: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp`,
  },
  {
    ball: 45.1,
    text: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp`,
  },
  {
    ball: 45.2,
    text: `Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp`,
  },
];

const CommentaryItem = ({ item }) => (
  <>
    <div className="d-flex" style={{ padding: 4 }}>
      <h5 className="m-0 ml-1 mr-1">{item.ball}</h5>
      <h6 className="m-0">{item.text}</h6>
    </div>
    <hr />
  </>
);

const Commentary = () => {
  return (
    <Row>
      <Col lg="12" md="12">
        {data.map((item, i) => (
          <CommentaryItem item={item} key={i} />
        ))}
      </Col>
    </Row>
  );
};

export default Commentary;
