import React, { useState } from 'react';
import MatchDetails from './MatchDetails';
import Tabs from './Tabs';

const TPMatch = () => {
  const buttonTypes = ['Main', 'Innings', 'Players', 'Special'];
  const [selectedIndex, setSelectedIndex] = useState(0);

  const onTabPress = (index) => {
    setSelectedIndex(index);
  };

  return (
    <>
      <Tabs
        items={buttonTypes}
        onPress={onTabPress}
        selectedIndex={selectedIndex}
      />
      <MatchDetails />
    </>
  );
};

export default TPMatch;
