import React from 'react';
import PredictionListItem from './PredictionListItem';

const MatchDetails = () => {
  return (
    <>
      {[...Array(5).keys()].map((item) => (
        <PredictionListItem />
      ))}
    </>
  );
};

export default MatchDetails;
