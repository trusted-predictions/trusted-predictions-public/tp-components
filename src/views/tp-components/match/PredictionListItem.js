import React from 'react';
import { Button, Card } from 'reactstrap';

const PredictionListItem = () => {
  return (
    <Card className="p-1 mb-1">
      <div className="d-flex justify-content-between">
        <h4 className="m-0">Winner Prediction</h4>
        <h5 className="m-0">50 Coins</h5>
      </div>
      <hr style={{ marginTop: 10 }} />
      <div className="d-flex justify-content-between align-items-center">
        <h5 className="m-0">
          Confidence : <span className="text-primary">67%</span>
        </h5>
        <Button color="primary" outline>
          Buy Prediction
        </Button>
      </div>
    </Card>
  );
};

export default PredictionListItem;
