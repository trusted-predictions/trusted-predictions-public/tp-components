import React from 'react';
import PropTypes from 'prop-types';

const Tabs = ({ items, onPress, selectedIndex }) => {
  return (
    <>
      <div className="d-flex flex-wrap">
        {items.map((item, index) => (
          <button
            onClick={() => onPress(index)}
            className={`mr-1 p-1 btn rounded-0 ${
              selectedIndex === index && 'border-bottom-primary'
            }`}
          >
            {selectedIndex === index ? (
              <h5 className="m-0 mt-0 text-primary">{item}</h5>
            ) : (
              <h6 className="m-0 mt-0 text-dark">{item}</h6>
            )}
          </button>
        ))}
      </div>
      <hr className="mt-0" />
    </>
  );
};

Tabs.defaultProps = {
  onPress: () => {},
  selectedIndex: 0,
};

Tabs.propTypes = {
  items: PropTypes.array.isRequired,
  onPress: PropTypes.func,
  selectedIndex: PropTypes.number,
};

export default Tabs;
