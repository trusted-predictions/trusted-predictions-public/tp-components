import React from 'react';

const ListItem = ({ selected }) => (
  <div className="d-flex flex-column align-items-center ml-1 mr-1 mt-1 cursor-pointer">
    <div
      style={{ height: 30, width: 30, borderRadius: 8 }}
      className={`bg-${selected ? 'primary' : 'light'}`}
    />
    <p style={{ margin: 0, padding: 4 }}>Game 1</p>
  </div>
);

const GamesList = () => {
  return (
    <div className="d-flex bg-white mb-2 flex-wrap w-100">
      {[...Array(8).keys()].map((item) => (
        <ListItem key={item} selected={item === 0} />
      ))}
    </div>
  );
};

export default GamesList;
