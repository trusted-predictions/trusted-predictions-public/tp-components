import React from 'react';
import Poster from './Poster';
import BaseMatchCard from './BaseMatchCard';
import MatchCard from './MatchCard';
import GamesSelector from './GamesSelector';
import GamesList from './GamesList';

const TPCard = () => (
  <>
    <Poster />
    <GamesSelector />
    <GamesList />
    <h5>Live Matches</h5>
    {[...Array(5).keys()].map((item) => (
      <MatchCard key={item} />
    ))}
    <BaseMatchCard />
  </>
);

export default TPCard;
