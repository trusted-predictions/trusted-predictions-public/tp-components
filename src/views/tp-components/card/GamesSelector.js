import React from 'react';

const DOT_SIZE = 24;

const SelectorItem = ({ selected, event }) => (
  <div className="bg-white d-flex align-items-center mb-1 p-1 cursor-pointer rounded mr-1">
    <div
      style={styles.dot}
      className={`mr-1 bg-${selected ? 'primary' : 'light'}`}
    />
    <h6 className="m-0">{event.name}</h6>
  </div>
);

const events = [
  { name: 'Live Events' },
  { name: 'Upcoming Events' },
  { name: 'Recent Events' },
];

const GamesSelector = () => {
  return (
    <div className="d-flex flex-wrap w-100 mb-2">
      {events.map((event, id) => (
        <SelectorItem event={event} key={id.toString()} selected={id === 0} />
      ))}
    </div>
  );
};

export default GamesSelector;

const styles = {
  dot: {
    height: DOT_SIZE,
    width: DOT_SIZE,
    borderRadius: DOT_SIZE,
  },
};
