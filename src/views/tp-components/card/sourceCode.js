import React from "react"
export const TeamCards = (
  <pre>
    <code className ="language-jsx">
      {`
      import React from "react"
      // Custom styles needs to be imported 
      import import "./Styles/style.css";
      export const TeamCard = () => {
        return(
            <div className="helper-teamCardMain">
                <div className="helper-teamName">
                    <b>Fantasy Team 1</b>
                </div>
                <div className="helper-scorePred">
                    <div className="helper-group helper-predicted">
                        <b>600</b>
                        <span>Predicted</span>
                    </div>
                    <div className="helper-group helper-scored">
                        <b>590</b>
                        <span>Scored</span>
                    </div>
                </div>
                <div className="helper-teamPlayers">
                    <div className="helper-teamPlayer">
                        <b>5 Player</b>
                        <span>Team A</span>
                    </div>
                    <div className="helper-teamPlayer">
                        <b>6 Player</b>
                        <span>Team B</span>
                    </div>
                </div>
            </div>
        );
    }
      `}
    </code>
  </pre>
)
export const PlayerCards = (
  <pre>
    <code className ="language-jsx">
      {`
      import React from "react"
      // Custom styles needs to be imported 
      import import "./Styles/style.css";
        export const PlayerCard = () =>{
          return(
              <div className="helper-playerCard">
                  <div className="helper-cardTop">
                      <div className="helper-playerName">
                          <span>Virat</span><br></br>
                          Kohli
                      </div>
                      <div className="helper-playerTeam">India</div>
                  </div>
                  <div className="helper-cardBody">
                      <div className="helper-statisticsRow"><span>Icc Rank</span><span>1st</span></div>
                      <div className="helper-statisticsRow"><span>Matches</span><span>500</span></div>
                      <div className="helper-statisticsRow"><span>Innings</span><span>496</span></div>
                      <div className="helper-statisticsRow"><span>Runs</span><span>20000</span></div>
                      <div className="helper-statisticsRow"><span>50/100s</span><span>43/48</span></div>
                  </div>
              </div>
          )
      }
      `}
    </code>
  </pre>
)
export const ScoreCards = (
    <pre>
      <code className="language-jsx">
        {`
  import React from "react"
  // Custom styles needs to be imported 
  import import "./Styles/style.css";
        
  export const ScoreCard = () =>{
    return(
        <div className="helper-container"> 
            <div className="helper-scoreCard">
                <div className="helper-top">
                    <b>India Tour of Australia</b><br></br>
                    <span>match 2 out of 5</span>
                </div>
                <div className="helper-teamsIcoNamesScores">
                    <div className="helper-teamIcoNameScore">
                        <div className="helper-teamImgName"><span className="helper-teamImg"><img
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAKlBMVEXg4OD////j4+Pb29v7+/vi4uLx8fHs7Oz39/f09PTa2tru7u7m5ubX19cF3ejnAAABRElEQVR4nO3Z27JDMBiAURFVVN//dXfp+ZC6Y0//tS4zphPflARVBQAAAAAAAAAAAAAAAAAAAAAAAADAj6i/23p6G+jSkn7rKa6tXUyS0mHrSa4rp9QuHNKlJq8yl//i1GS/cEgbtsm4Kx0StEme7ipd4ZCgTQ7zrbT7fOoxm+TL+vL58ondZLwPd/2tQ+wm99HTRu4WJWaTapyTtNdTz/Pe9holaJNcnxrsh+vYZbt/iRK0SVUdj8fnf8k9StgmDyMPD4VzlNBN5qU4Pz0nT1EiN2mmdSe/vDroQzdppsX4NUlKOXCT5ry9f3t3ErhJU3qfFLdJMUncJuUkYZvkchJNNJlo8u58P6l3JXXoPVtRxCZ9PX5Tx/u+4zvgu2H5e3E7LP/MbxmWLowhXBIAAAAAAAAAAAAAAAAAAAAAAAAAgF/1BxZSCIBLTls7AAAAAElFTkSuQmCC"
                                    alt="team-img" /></span><span className="helper-teamName">AUSTRALIA</span></div>
                        <div className="helper-teamScore">269 - 7</div>
                    </div>
                    <div className="helper-wonMatch">India Won match by 5 wickets</div>
                    <div className="helper-teamIcoNameScore">
                        <div className="helper-teamScore">269 - 7</div>
                        <div className="helper-teamImgName"><span className="helper-teamImg">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAKlBMVEXg4OD////j4+Pb29v7+/vi4uLx8fHs7Oz39/f09PTa2tru7u7m5ubX19cF3ejnAAABRElEQVR4nO3Z27JDMBiAURFVVN//dXfp+ZC6Y0//tS4zphPflARVBQAAAAAAAAAAAAAAAAAAAAAAAADAj6i/23p6G+jSkn7rKa6tXUyS0mHrSa4rp9QuHNKlJq8yl//i1GS/cEgbtsm4Kx0StEme7ipd4ZCgTQ7zrbT7fOoxm+TL+vL58ondZLwPd/2tQ+wm99HTRu4WJWaTapyTtNdTz/Pe9holaJNcnxrsh+vYZbt/iRK0SVUdj8fnf8k9StgmDyMPD4VzlNBN5qU4Pz0nT1EiN2mmdSe/vDroQzdppsX4NUlKOXCT5ry9f3t3ErhJU3qfFLdJMUncJuUkYZvkchJNNJlo8u58P6l3JXXoPVtRxCZ9PX5Tx/u+4zvgu2H5e3E7LP/MbxmWLowhXBIAAAAAAAAAAAAAAAAAAAAAAAAAgF/1BxZSCIBLTls7AAAAAElFTkSuQmCC" alt="team-img" /></span><span className="helper-teamName">INDIA</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
  `}
      </code>
    </pre>
  )



  export const ScoreCardCss = (
    <pre>
      <code className="language-jsx">
        {`
          /* helper rule sets */
          .helper-container{
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
        }
        .helper-scoreCard{
            border: 4px solid rgb(102, 141, 212);
            width: 50%;
            border-radius: 8px;
            text-align: center;
            padding:1%;
            box-shadow: 2px 5px 20px -5px rgb(175, 175, 175);
            .helper-top{
                b{
                    font-weight: 800;
                }
                span{
                    font-weight: 500;
                    color: rgb(167, 167, 167);
                    font-size: 0.8rem;
                }
            }
        
        .helper-teamsIcoNamesScores{
            margin-top: 30px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            .helper-teamIcoNameScore{
                display: flex;
                align-items: center;
                width: 30%;
                .helper-teamScore{
                    margin-bottom: 30px;
                    font-size: 0.85rem;
                    font-weight: 500;
                    color: rgb(102, 102, 102);
                }
                .helper-teamImgName{
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    width: 60%;
                    .helper-teamImg{
                        height: auto;
                        width: 80%;
                        
                        img{
                            max-width: 100%;
                        }
                    }
                    .helper-teamName{
                        margin-top: 5px;
                        font-size: 0.7rem;
                        font-weight: 500;
                    }
                }
            }
            .helper-wonMatch{
                margin-top: 20px;
                font-weight: 500;
                font-size: 0.65rem;
                color: rgb(90, 209, 90);
            }
        }
        }
        
        @media only screen and (max-width: 600px){
            .helper-scoreCard{
                border: 4px solid rgb(102, 141, 212);
                width: 100%;
            }
        }   
        `}
      </code>
    </pre>
  )

  
  export const PlayerCardCss = (
    <pre>
      <code className="language-jsx">
        {`
          /* helper rule sets */
          .helper-container{
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
        }
        .helper-playerCard{
          margin-top: 20px;
          width: 20%;
          border-radius: 10px;
          text-align: center;
          box-shadow: 2px 5px 20px -5px rgb(175, 175, 175);
          .helper-cardTop{
              margin-top: 10px;
              display: flex;
              justify-content: space-between;
              align-items: flex-start;
              .helper-playerName{
                  text-align: left;
                  margin-left: 8px;
                  width: 70%;
                  font-size: 2rem;
                  span{
                      font-weight: 700;
                  }
              }
              .helper-playerTeam{
                  margin-top: 10px;
                  margin-right: 8px;
                  font-weight: 500;
                  font-size: 0.8rem;
                  color: rgb(32, 108, 221);
              }
          }
          .helper-cardBody{
              margin-top: 10px;
              padding: 5px 0 15px 0;
              border-radius: 0 0 10px 10px;
              background: rgb(0,0,0);
              background: linear-gradient(0deg, rgba(0,0,0,1) 20%, rgba(85,85,85,1) 65%);
              color: white;
              font-size: 0.8rem;
              font-weight: 500;
              .helper-statisticsRow{
                  width: 90%;
                  margin: 10px auto 0 auto;
                  display: flex;
                  justify-content: space-between;
                  span:nth-child(2){
                      color: green;
                  }
          
              }
          }
          }
          
          @media only screen and (max-width: 600px){
              .helper-playerCard{
                  margin-top: 20px;
                  width: 40%;
              }
          }  
        `}
      </code>
    </pre>
  )
  
  export const TeamCardCss = (
    <pre>
      <code className="language-jsx">
        {`
          /* helper rule sets */
          .helper-container{
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
        }
        .helper-teamCardMain{
          display: flex;
          align-items: center;
          margin-top: 20px;
      border-radius: 15px;
      text-align: center;
      box-shadow: 2px 5px 20px -5px rgb(175, 175, 175);
      padding: 1%;
      width: 60%;
      .helper-teamName{
          border-right: 2px solid rgb(233, 233, 233);
          width: 30%;
          min-height: 35px;
          display: flex;
          align-items: center;
          justify-content: center;
      }
      .helper-scorePred{
          display: flex;
          border-right: 2px solid rgb(223, 223, 223);
          width: 30%;
          justify-content: center;
      
          .helper-group{
              display: flex;
              flex-direction: column;
              margin: 0 10px;
              span{
                  font-size: 0.7rem;
                  color: rgb(82, 187, 82);
                  font-weight: 500;
              }
          }
      }
      .helper-teamPlayers{
          display: flex;
          width: 40%;
          justify-content: center;
          .helper-teamPlayer{
              display: flex;
              flex-direction: column;
              margin: 0 10px;
              span{
                  font-size: 0.7rem;
                  color: rgb(82, 187, 82);
                  font-weight: 500;
              }
          }
      }
      }
      @media only screen and (max-width: 600px){
          .helper-teamCardMain{
               width: 100%;
               font-size: 0.7rem;
               span{
                  font-size: 0.65rem !important;
              }
          }
      }  
        `}
      </code>
    </pre>
  )




