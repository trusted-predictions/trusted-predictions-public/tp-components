import React from 'react';
import { Card } from 'reactstrap';

const Poster = () => {
  return (
    <Card
      style={{ height: 240 }}
      className="d-flex d-flex align-items-center justify-content-center poster poster"
    >
      <h4 className="text-center">poster is here</h4>
    </Card>
  );
};

export default Poster;
