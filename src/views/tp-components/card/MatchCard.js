import React from 'react';
import { Button, Card, Col, Row } from 'reactstrap';
import classNames from 'classnames';

const SCORE_WIDTH = window.innerWidth > 570 ? 350 : 260;

const PredictionCard = () => (
  <div
    style={{
      height: 34,
      borderRadius: 10,
      backgroundColor: 'grey',
      marginBottom: 8,
    }}
  />
);

const inningsData = [
  {
    name: 'india',
    score: '134-2',
  },
  {
    name: 'Australis',
    score: 'yet to bat',
  },
];

const MatchCard = () => {
  return (
    <Card className="p-1">
      <div className="d-flex justify-content-between align-items-baseline mb-1">
        <div
          className="d-flex justify-content-between"
          style={{ width: SCORE_WIDTH }}
        >
          <p className="text-dark">India tour of australia</p>
          <p className="text-danger">Match 3 of 5</p>
        </div>
        <Button
          className={classNames({ 'p-1': window.innerWidth < 570 })}
          size={window.innerWidth > 570 ? 'md' : 'sm'}
          outline
          color="primary"
        >
          View All
        </Button>
      </div>

      <Row>
        <Col md="6" xs="12">
          <div className="d-flex justify-content-between mb-1">
            <p>First innings</p>
            <p>40.3</p>
          </div>
          {inningsData.map((item) => (
            <div
              key={item.name}
              className="d-flex justify-content-between mb-1"
            >
              <h5 className="text-dark">{item.name}</h5>
              <h5 className="text-danger">{item.score}</h5>
            </div>
          ))}
        </Col>
        <Col md="6" xs="12">
          <p>Predictions</p>
          <Row style={{}} className="d-flex justify-content-between">
            <Col xs="6">
              <PredictionCard />
            </Col>
            <Col xs="6">
              <PredictionCard />
            </Col>
          </Row>
          <Row className="d-flex justify-content-between">
            <Col xs="6">
              <PredictionCard />
            </Col>
            <Col xs="6">
              <PredictionCard />
            </Col>
          </Row>
        </Col>
      </Row>
    </Card>
  );
};

export default MatchCard;
