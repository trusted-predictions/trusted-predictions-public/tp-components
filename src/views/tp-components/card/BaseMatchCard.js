import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  CardHeader,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import { Eye, Code, Hash } from 'react-feather';
import classnames from 'classnames';
import {
  ScoreCards,
  PlayerCards,
  TeamCards,
  ScoreCardCss,
  PlayerCardCss,
  TeamCardCss,
} from './sourceCode';
import { ScoreCard, PlayerCard, TeamCard } from './CountryMatchCard';

class BaseMatchCard extends React.Component {
  state = {
    activeTab: '1',
  };

  toggleTab = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({ activeTab: tab });
    }
  };
  render() {
    return (
      <React.Fragment>
        <Card>
          <CardHeader>
            <CardTitle>Base Match Card</CardTitle>
            <div className="views">
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '1',
                    })}
                    onClick={() => {
                      this.toggleTab('1');
                    }}
                  >
                    <Eye size={15} />
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '2',
                    })}
                    onClick={() => {
                      this.toggleTab('2');
                    }}
                  >
                    <Code size={15} />
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '3',
                    })}
                    onClick={() => {
                      this.toggleTab('3');
                    }}
                  >
                    <Hash size={15} />
                  </NavLink>
                </NavItem>
              </Nav>
            </div>
          </CardHeader>
          <CardBody>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
                <ScoreCard />
              </TabPane>
              <TabPane className="component-code" tabId="2">
                {ScoreCards}
              </TabPane>
              <TabPane className="component-code" tabId="3">
                {ScoreCardCss}
              </TabPane>
            </TabContent>
          </CardBody>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Player Card</CardTitle>
            <div className="views">
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '4',
                    })}
                    onClick={() => {
                      this.toggleTab('4');
                    }}
                  >
                    <Eye size={15} />
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '5',
                    })}
                    onClick={() => {
                      this.toggleTab('5');
                    }}
                  >
                    <Code size={15} />
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '6',
                    })}
                    onClick={() => {
                      this.toggleTab('6');
                    }}
                  >
                    <Hash size={15} />
                  </NavLink>
                </NavItem>
              </Nav>
            </div>
          </CardHeader>
          <CardBody>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="4">
                <PlayerCard />
              </TabPane>
              <TabPane className="component-code" tabId="5">
                {PlayerCards}
              </TabPane>
              <TabPane className="component-code" tabId="6">
                {PlayerCardCss}
              </TabPane>
            </TabContent>
          </CardBody>
        </Card>

        <Card>
          <CardHeader>
            <CardTitle>Team Card</CardTitle>
            <div className="views">
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '7',
                    })}
                    onClick={() => {
                      this.toggleTab('7');
                    }}
                  >
                    <Eye size={15} />
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '8',
                    })}
                    onClick={() => {
                      this.toggleTab('8');
                    }}
                  >
                    <Code size={15} />
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === '9',
                    })}
                    onClick={() => {
                      this.toggleTab('9');
                    }}
                  >
                    <Hash size={15} />
                  </NavLink>
                </NavItem>
              </Nav>
            </div>
          </CardHeader>
          <CardBody>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="7">
                <TeamCard />
              </TabPane>
              <TabPane className="component-code" tabId="8">
                {TeamCards}
              </TabPane>
              <TabPane className="component-code" tabId="9">
                {TeamCardCss}
              </TabPane>
            </TabContent>
          </CardBody>
        </Card>
      </React.Fragment>
    );
  }
}

export default BaseMatchCard;
