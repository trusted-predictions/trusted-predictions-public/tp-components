import React from "react";
import "./Styles/style.css";

export const ScoreCard = () =>{
    return(
        <div className="helper-container"> 
            <div className="helper-scoreCard">
                <div className="helper-top">
                    <b>India Tour of Australia</b><br></br>
                    <span>match 2 out of 5</span>
                </div>
                <div className="helper-teamsIcoNamesScores">
                    <div className="helper-teamIcoNameScore">
                        <div className="helper-teamImgName"><span className="helper-teamImg"><img
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAKlBMVEXg4OD////j4+Pb29v7+/vi4uLx8fHs7Oz39/f09PTa2tru7u7m5ubX19cF3ejnAAABRElEQVR4nO3Z27JDMBiAURFVVN//dXfp+ZC6Y0//tS4zphPflARVBQAAAAAAAAAAAAAAAAAAAAAAAADAj6i/23p6G+jSkn7rKa6tXUyS0mHrSa4rp9QuHNKlJq8yl//i1GS/cEgbtsm4Kx0StEme7ipd4ZCgTQ7zrbT7fOoxm+TL+vL58ondZLwPd/2tQ+wm99HTRu4WJWaTapyTtNdTz/Pe9holaJNcnxrsh+vYZbt/iRK0SVUdj8fnf8k9StgmDyMPD4VzlNBN5qU4Pz0nT1EiN2mmdSe/vDroQzdppsX4NUlKOXCT5ry9f3t3ErhJU3qfFLdJMUncJuUkYZvkchJNNJlo8u58P6l3JXXoPVtRxCZ9PX5Tx/u+4zvgu2H5e3E7LP/MbxmWLowhXBIAAAAAAAAAAAAAAAAAAAAAAAAAgF/1BxZSCIBLTls7AAAAAElFTkSuQmCC"
                                    alt="team-img" /></span><span className="helper-teamName">AUSTRALIA</span></div>
                        <div className="helper-teamScore">269 - 7</div>
                    </div>
                    <div className="helper-wonMatch">India Won match by 5 wickets</div>
                    <div className="helper-teamIcoNameScore">
                        <div className="helper-teamScore">269 - 7</div>
                        <div className="helper-teamImgName"><span className="helper-teamImg">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAKlBMVEXg4OD////j4+Pb29v7+/vi4uLx8fHs7Oz39/f09PTa2tru7u7m5ubX19cF3ejnAAABRElEQVR4nO3Z27JDMBiAURFVVN//dXfp+ZC6Y0//tS4zphPflARVBQAAAAAAAAAAAAAAAAAAAAAAAADAj6i/23p6G+jSkn7rKa6tXUyS0mHrSa4rp9QuHNKlJq8yl//i1GS/cEgbtsm4Kx0StEme7ipd4ZCgTQ7zrbT7fOoxm+TL+vL58ondZLwPd/2tQ+wm99HTRu4WJWaTapyTtNdTz/Pe9holaJNcnxrsh+vYZbt/iRK0SVUdj8fnf8k9StgmDyMPD4VzlNBN5qU4Pz0nT1EiN2mmdSe/vDroQzdppsX4NUlKOXCT5ry9f3t3ErhJU3qfFLdJMUncJuUkYZvkchJNNJlo8u58P6l3JXXoPVtRxCZ9PX5Tx/u+4zvgu2H5e3E7LP/MbxmWLowhXBIAAAAAAAAAAAAAAAAAAAAAAAAAgF/1BxZSCIBLTls7AAAAAElFTkSuQmCC" alt="team-img" /></span><span className="helper-teamName">INDIA</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export const PlayerCard = () =>{
    return(
        <div className="helper-playerCard">
            <div className="helper-cardTop">
                <div className="helper-playerName">
                    <span>Virat</span><br></br>
                    Kohli
                </div>
                <div className="helper-playerTeam">India</div>
            </div>
            <div className="helper-cardBody">
                <div className="helper-statisticsRow"><span>Icc Rank</span><span>1st</span></div>
                <div className="helper-statisticsRow"><span>Matches</span><span>500</span></div>
                <div className="helper-statisticsRow"><span>Innings</span><span>496</span></div>
                <div className="helper-statisticsRow"><span>Runs</span><span>20000</span></div>
                <div className="helper-statisticsRow"><span>50/100s</span><span>43/48</span></div>
            </div>
        </div>
    )
}


export const TeamCard = () => {
    return(
        <div className="helper-teamCardMain">
            <div className="helper-teamName">
                <b>Fantasy Team 1</b>
            </div>
            <div className="helper-scorePred">
                <div className="helper-group helper-predicted">
                    <b>600</b>
                    <span>Predicted</span>
                </div>
                <div className="helper-group helper-scored">
                    <b>590</b>
                    <span>Scored</span>
                </div>
            </div>
            <div className="helper-teamPlayers">
                <div className="helper-teamPlayer">
                    <b>5 Player</b>
                    <span>Team A</span>
                </div>
                <div className="helper-teamPlayer">
                    <b>6 Player</b>
                    <span>Team B</span>
                </div>
            </div>
        </div>
    );
}