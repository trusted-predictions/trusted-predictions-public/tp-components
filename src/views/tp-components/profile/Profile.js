import React, { useState } from 'react';
import { Button, Media } from 'reactstrap';
import Tabs from '../match/Tabs';
import { Card, Col, Row } from 'reactstrap';
import userImg from '../../../assets/img/portrait/small/avatar-s-18.jpg';
import * as Icon from 'react-feather';

const options = ['General', 'Notifications', 'Preferences', 'Other'];

const Profile = () => {
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [userFormData, setUserFormData] = useState({
    name: 'Caesar Ortiz',
    username: 'C342355',
    email: 'test@example.com',
    password: 'password',
  });

  const { name, username, email, password } = userFormData;

  const onTabPress = (index) => {
    setSelectedIndex(index);
  };

  const onChangeText = (e) => {
    setUserFormData({
      ...userFormData,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <>
      <div className="d-flex justify-content-between align-items-center">
        <h3 className="m-0">Settings</h3>
        <Button color="primary">Back</Button>
      </div>
      <hr />
      <Tabs
        items={options}
        onPress={onTabPress}
        selectedIndex={selectedIndex}
      />
      <Card className="p-1">
        <Row>
          <Col xs="12" sm="4" md="4" lg="3">
            <div className="d-flex justify-content-center">
              <Media
                className="rounded-circle"
                object
                src={userImg}
                alt="Generic placeholder image"
                height="112"
                width="112"
              />
            </div>
          </Col>
          <Col xs="12" sm="8" md="8" lg="9" className="pl-1">
            <h1 className="font-weight-bold m-0 mt-1 text-left">
              Caesar Ortiz
            </h1>
            <h5 className="text-danger text-left" style={{ marginTop: 8 }}>
              test@email.com
            </h5>
            <h6
              className="text-light text-left font-weight-bold cursor-pointer"
              style={{ marginTop: 8 }}
            >
              Change profile picture
            </h6>
            <hr />
          </Col>
          <Col xs="12">
            <form>
              <div className="form-group">
                <label for="name" className="d-block label">
                  Name
                </label>
                <input
                  className="w-100 input"
                  type="text"
                  id="name"
                  name="name"
                  required
                  placeholder="Enter name"
                  value={name}
                  onChange={(e) => onChangeText(e)}
                />
                <Icon.Edit2 size={20} className="input-icon text-primary" />
              </div>
              <div className="form-group">
                <label for="username" className="d-block label">
                  Username
                </label>
                <input
                  className="w-100 input"
                  type="text"
                  id="username"
                  name="username"
                  required
                  placeholder="Enter username"
                  value={username}
                  onChange={(e) => onChangeText(e)}
                />
                <Icon.Edit2 size={20} className="input-icon text-primary" />
              </div>
              <div className="form-group">
                <label for="email" className="d-block label">
                  Email
                </label>
                <input
                  className="w-100 input"
                  type="email"
                  id="email"
                  name="email"
                  required
                  placeholder="Enter email"
                  value={email}
                  onChange={(e) => onChangeText(e)}
                />
                <Icon.Edit2 size={20} className="input-icon text-primary" />
              </div>
              <div className="form-group">
                <label for="password" className="d-block label">
                  Password
                </label>
                <input
                  className="w-100 input"
                  type="password"
                  id="password"
                  name="password"
                  required
                  placeholder="Enter password"
                  value={password}
                  onChange={(e) => onChangeText(e)}
                />
                <Icon.Edit2 size={20} className="input-icon text-primary" />
              </div>
            </form>
          </Col>
        </Row>
      </Card>
    </>
  );
};

export default Profile;
