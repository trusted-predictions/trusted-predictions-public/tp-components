# Trusted Predictions Components

ReactJS Components

### Run Project
>npm i  
>npm start

### How to develop new components
1. Create a component view in src/views/tp-components/{type}/{name}
2. Define the route in src/Route.js
3. Show component in sidebar in src/config/navigationConfig
